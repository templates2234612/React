import React from 'react';
import ReactDOM from 'react-dom/client';

const root = ReactDOM.createRoot(document.getElementById('root')!);
root.render(<h1>Hello, dirty world!</h1>);

const mu: number = 123;