
const path = require('path');

const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

const PATHS = {
	src: path.resolve(__dirname, '../src'),
	public: path.resolve(__dirname, '../public'),
	assets: '/assets'
}

module.exports = {
	externals: {
		path: PATHS,
	},
	entry: {
		main: path.join(PATHS.src),
	},
	output: {
		filename: `./js/build.js`,
		path: PATHS.public,
		// publicPath: '/',
		clean: true,
	},
	module: {
		rules: [
			// {
			// 	test: /\.(ts|js)x?$/,
			// 	loader: 'ts-loader',
      //   exclude: /node_modules/,
      // },
			{
				test: /\.(ts|js)x?$/,
				loader: 'babel-loader'
			},
			{
				test: /\.(sc|sa|c)ss$/,
				use: [
					MiniCssExtractPlugin.loader,
					{
						loader: 'css-loader',
						options: { sourceMap: true }
					},
					{
						loader: 'postcss-loader',
						options: { 
							sourceMap: true,
							postcssOptions: {
								plugins: [
									[
										"postcss-preset-env",
										require('autoprefixer'),
										require('css-mqpacker'),
										require('cssnano')({
											preset: [
												'default', {
													discardComments: {
														removeAll: true,
													}
												}
											]
										}),
									],
								],
							}
						}
					},
					{
						loader: 'sass-loader',
						options: { sourceMap: true }
					},
				],
			},
			{
				test: /\.(woff|woff2|ttf)$/,
				type: 'asset/resource',
        generator: {
          filename: 'assets/fonts/[name][ext]',
        },
			}
		],
	},
	resolve: {
    extensions: ['.tsx', '.ts', '.js', '.d.ts'],
  },
	plugins: [
		new MiniCssExtractPlugin({
			filename: `./css/build.css`,
		}),
		new HtmlWebpackPlugin({
			hash: false,
			template: `${PATHS.src}/index.html`,
			filename: './index.html'
		}),
		new CopyPlugin({
      patterns: [
        { from: `${PATHS.src}/assets/images`, to: `${PATHS.public + PATHS.assets}/images` },
      ],
    }),
	]
}