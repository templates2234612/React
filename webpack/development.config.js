const webpack = require('webpack');
const { merge } = require('webpack-merge');
const config_general = require('./config');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');

const config_dev = merge(config_general, {
	mode: 'development',
	devServer: {
    static: {
      directory: config_general.externals.path.public,
    },
    compress: true,
		hot: true,
    port: 9000,
  },
  plugins: [
    new ForkTsCheckerWebpackPlugin(),
  ]
})


module.exports = new Promise((resolve, reject) => {
	resolve(config_dev);
});